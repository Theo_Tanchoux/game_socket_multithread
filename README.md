# 6 qui prend 

## Dev logiciel avancé

- [A propos du projet](#a-propos-du-projet)
- [Pré-requis](#pré-requis)
- [Fonctionnement](#fonctionnement)
- [Installer](#installer)
- [Choix de techno](#choix-de-techno)

## A propos du projet

Projet sur le 6 qui prend

### Serveur

- C++
- Utilise les threads
- Communique par socket
- Une partie avec 2 à 10 joueurs
- Règles du jeu
- Traite les demandes client

### Client

- Implémente les actions qu'un joueur peut faire et les envoie au serveur
- Communique par socket
- Vue.js et electronjs

## Pré-requis

Serveur compilable sur linux uniquement

## Fonctionnement

### START : 

- Initie la partie, la partie ne peut commencer qu'à partir de 2 joueurs

### ACK_PLAY :

- Signifie que le client est prêt à jouer et a compris les règles du jeu.

### BOARD

- Retourne le plateau de jeu avec ses cartes par ligne, un tableau de Card pour chaque ligne sérialisé en JSON 

### PLAY_CARDS

- Envoyer une Card à valider pour le tour avec le nombre de la carte en paramètre

```bash
  PLAY_CARDS86
```

### LINE

- Récupère une ligne de carte pour le joueur

### DISCONNECT

- Termine la connexion avec le serveur

## Installer

- Clonez ce repo et celui du [client](https://gitlab.com/HugoTysn21/game-socket)

### Back

- Dans un terminal :

```bash
  make
  ./bin/main 127.0.0.1 YOUR_PORT
```

### Front

```bash
  npm install
  npm run electron:serve
```

## Choix de techno

Nous avons choisi electron étant donné que nous connaissions le JS par rapport au C# et WPF n'étant pas compatible macOS, il ne semblait pas raisonnable de coder sur une VM.
L'intégration d'un container docker est compliquée car c'est un processus sous-jacents de vue-js et qu'il n'utilise pas les meme processus. l'un est un processus nodeJS et l'autre vueJS.
