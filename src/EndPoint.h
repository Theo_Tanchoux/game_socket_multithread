
#ifdef _WIN32
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#include <winsock2.h>
#pragma comment(lib, "ws2_32.lib")
#else
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif
#include <thread>
#include <vector>
#include <optional>
#include "Card.h"
#include "JSON.h"

class Client;

#ifndef PC_H
#define PC_H
struct PLAYING_CARD
{
	Client *client;
	Card *card;
	bool operator==(const Client &) const;
};
#endif

#ifndef ENDPOINT_H
#define ENDPOINT_H
class EndPoint
{
private:
	const int MAXDATASIZE;
	const int BACKLOG;
	int connection_port;
	bool is_alive;
	bool init_winsocks;
	int connection_socket;
	thread thread;
	vector<Client *> clients;
	vector<Client *> incoming_players;
	vector<Client *> players;
	vector<Card *> cards;
	vector<PLAYING_CARD> playingCards;
	condition_variable playerChoosing;
	mutex waitClient;
	unique_lock<mutex> myLock;
	bool lkClientChoosing = false;
	int lineId = 0;

	int open();
	int accept_connection();
	bool close();
	void execute_thread();
	optional<__1::chrono::time_point<__1::chrono::system_clock, __1::chrono::system_clock::duration>> startTimer;

public:
	EndPoint(int, const int, const int, bool);
	~EndPoint();

	void start_thread();
	void end_thread();
	void join_thread();
	bool addPlayer(Client *);
	bool canGameStart();
	bool isPlayer(Client *);
	void launchGame();
	void launchTimer();
	void startGame(Client *);
	string createBoard();
	void initCards();
	void assignPlayerCard(Client *);

	vector<vector<Card *>> board;

	void playCard(int, Client *);
	void sendBoard(Client * = nullptr);
	void sendCards(Client * = nullptr);

	void chooseLine(int);

	bool isStartTimerEnded();

	void endPlayer(Client *);

    void sendScore(Client *);
};

#endif
