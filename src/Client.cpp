#ifdef _WIN32
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#include <winsock2.h>
#pragma comment(lib, "ws2_32.lib")
#else
#include <cstdio>
#include <unistd.h>
#include <string>
#include <sys/socket.h>
#include <ctime>
#endif
#include <thread>
#include <sstream>

#include "Client.h"
#include "Output.h"
#include "EndPoint.h"

#ifdef _WIN32
Client::Client(int id, SOCKET socket, const int MAXDATASIZE, EndPoint *server) : id(id), socket(socket), MAXDATASIZE(MAXDATASIZE), is_alive(true), server(server), isReady(false)
{
	buffer = new char[MAXDATASIZE];
}
#else

using namespace std;

Client::Client(int id, int socket, const int MAXDATASIZE, EndPoint *server) : id(id), socket(socket), MAXDATASIZE(MAXDATASIZE), is_alive(true), server(server), isReady(false)
{
	buffer = new char[MAXDATASIZE];
}
#endif

Client::~Client()
{
	end_thread();
	delete[] buffer;
}

bool Client::close_socket()
{
	if (socket == NULL || !is_alive)
		return true;

	int result;
	Output::GetInstance()->print("[CLIENT_", id, "] Closing client socket...\n");

#ifdef _WIN32
	result = closesocket(socket);
#else
	result = close(socket);
#endif

	if (result == -1)
	{
		char *error = new char[MAXDATASIZE];
		sprintf(error, "[CLIENT_%d] Error while closing socket ", id);
		Output::GetInstance()->print_error(error);
		Output::GetInstance()->print("\n");
		delete[] error;
		return false;
	}
	else
	{
		Output::GetInstance()->print("[CLIENT_", id, "] Client socket closed successfully.\n");
	}

	return true;
}
bool Client::send_message(const char *buffer)
{
	if (socket == NULL || !is_alive)
		return false;

	if (send(socket, buffer, strlen(buffer), 0) == -1)
	{
		char *error = new char[MAXDATASIZE];
		sprintf(error, "[CLIENT_%d] Error while sending message to client ", id);
		Output::GetInstance()->print_error(error);
		Output::GetInstance()->print("\n");
		delete[] error;
		return false;
	}

	return true;
}
int Client::recv_message()
{
	if (socket == NULL || !is_alive)
		return -1;

	int length;
	if ((length = recv(socket, buffer, MAXDATASIZE, 0)) == -1)
	{
		char *error = new char[MAXDATASIZE];
		sprintf(error, "[CLIENT_%d] Error while receiving message from client ", id);
		Output::GetInstance()->print_error(error);
		Output::GetInstance()->print("\n");
		delete[] error;
		return length;
	}

	// Suppression des retours chariots (\n et \r)
	while (length > 0 && (buffer[length - 1] == '\n' || buffer[length - 1] == '\r'))
		length--;
	// Ajout de backslash zero a la fin pour en faire une chaine de caracteres
	if (length >= 0 && length < MAXDATASIZE)
		buffer[length] = '\0';

	return length;
}

void Client::execute_thread()
{
	int length;
	time_t time_value;
	struct tm *time_info;

	Output::GetInstance()->print("[CLIENT_", id, "] Thread client starts with id=", id, ".\n");

	// Gestion unitaire des clients (thread / client)
	// Boucle infinie pour le client
	while (1)
	{

		if (socket == NULL || !is_alive)
			return;

		// On attend un message du client
		if ((length = recv_message()) == -1)
		{
			break;
		}

		if (socket == NULL || !is_alive)
			return;

		// Affichage du message
		Output::GetInstance()->print("[CLIENT_", id, "] Message received : ", buffer, "\n");

		if (strcmp(buffer, "DISCONNECT") == 0)
		{
			server->endPlayer(this);
			break;
		}
		else
		{
			string strBuf(buffer);
			if (strcmp(buffer, "START") == 0)
			{
				if (server->addPlayer(this) && server->isStartTimerEnded())
				{
					if (!(server->isPlayer(this) && !server->canGameStart()))
					{
						server->launchTimer();
					}
				}
			}
			else if (strcmp(buffer, "ACK_PLAY") == 0)
			{
				server->startGame(this);
			}
			else if (strcmp(buffer, "BOARD") == 0)
			{
				server->sendBoard(this);
			}
            else if (strcmp(buffer, "CARDS") == 0)
            {
                server->sendCards(this);
            }
            else if (strcmp(buffer, "SCORE") == 0)
            {
                server->sendScore(this);
            }
			else if (strBuf.find("PLAY_CARDS") != string::npos)
			{
				string delimiter = "PLAY_CARDS";
				server->playCard(stoi(strBuf.erase(0, strBuf.find(delimiter) + delimiter.length())), this);
			}
			else if (strBuf.find("LINE") != string::npos)
			{
				string delimiter = "LINE";
				server->chooseLine(stoi(strBuf.erase(0, strBuf.find(delimiter) + delimiter.length())));
			}
			else
				sprintf(buffer, "%s is not recognized as a valid command", buffer);

			if (socket == NULL || !is_alive)
				return;
		}
	}

	end_thread();
}

void Client::start_thread()
{
	join_thread();
	// Start client thread
	thread = std::thread(&Client::execute_thread, this);
}

void Client::end_thread()
{
	if (!is_alive)
		return;

	Output::GetInstance()->print("[CLIENT_", id, "] Thread client is ending...\n");

	// Sending close connection to client
	send_message(JSON::serialize("CONNECTION_CLOSED", "").c_str());

	is_alive = false;

	// End thread
	thread.detach();
	thread.~thread();

	// Close connection
	close_socket();

	Output::GetInstance()->print("[CLIENT_", id, "] Thread client ends.\n");
}

void Client::join_thread()
{
	if (thread.joinable())
	{
		thread.join();
	}
}
