#include "JSON.h"
using namespace rapidjson;
JSON *JSON::singleton_ = nullptr;

JSON *JSON::GetInstance()
{
    if (singleton_ == nullptr)
    {
        singleton_ = new JSON();
    }
    return singleton_;
}

JSON::JSON() = default;

string removeEscapedChar(string str)
{
    string endLine = "\n";
    string tab = " ";

    size_t pos = std::string::npos;
    while ((pos = str.find(endLine)) != std::string::npos)
    {
        str.erase(pos, endLine.length());
    }
    while ((pos = str.find(tab)) != std::string::npos)
    {
        str.erase(pos, tab.length());
    }
    return str;
}

string JSON::serialize(const char *event, const char *value)
{
    Document document;
    document.SetObject();

    Value eventField;
    eventField.SetString(event, strlen(event), document.GetAllocator());

    Value valueField;
    valueField.SetString(value, strlen(value), document.GetAllocator());

    document.AddMember("event", eventField, document.GetAllocator());
    document.AddMember("value", valueField, document.GetAllocator());

    StringBuffer sb;
    PrettyWriter<StringBuffer> writer(sb);
    document.Accept(writer);

    return removeEscapedChar(sb.GetString());
}

string JSON::serializeCard(Card card)
{
    Document document;
    document.SetObject();

    Value nbField;
    nbField.SetString(to_string(card.getNbCard()).c_str(), to_string(card.getNbCard()).size(), document.GetAllocator());

    Value headField;
    headField.SetString(to_string(card.getHead()).c_str(), to_string(card.getHead()).size(), document.GetAllocator());

    document.AddMember("nb", nbField, document.GetAllocator());
    document.AddMember("head", headField, document.GetAllocator());
    StringBuffer sb;
    PrettyWriter<StringBuffer> writer(sb);
    document.Accept(writer);

    return removeEscapedChar(sb.GetString());
}

string JSON::serializeCards(vector<Card *> cards)
{
    Document document;
    document.SetArray();

    for (Card *card : cards)
    {
        Value field(kObjectType);
        Value nbField(card->getNbCard());

        Value headField(card->getHead());

        field.AddMember("nb", nbField, document.GetAllocator());
        field.AddMember("head", headField, document.GetAllocator());
        document.PushBack(field, document.GetAllocator());
    }
    StringBuffer sb;
    PrettyWriter<StringBuffer> writer(sb);
    document.Accept(writer);

    return removeEscapedChar(sb.GetString());
}

string JSON::serializeBoard(vector<vector<Card *>> board)
{
    Document document;
    document.SetArray();

    for (auto line : board)
    {
        Value cardList(kArrayType);

        for (Card *card : line)
        {
            Value field(kObjectType);
            Value nbField(card->getNbCard());

            Value headField(card->getHead());

            field.AddMember("nb", nbField, document.GetAllocator());
            field.AddMember("head", headField, document.GetAllocator());
            cardList.PushBack(field, document.GetAllocator());
        }

        document.PushBack(cardList, document.GetAllocator());
    }

    StringBuffer sb;
    PrettyWriter<StringBuffer> writer(sb);
    document.Accept(writer);

    return removeEscapedChar(sb.GetString());
}