#include <stdlib.h>
#include <sys/socket.h>
#include <time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <chrono>
#include <sstream>
#include <random>
#include <iterator>
#include <mutex>
#include <future>
#include "EndPoint.h"
#include "Client.h"
#include "Output.h"

template <typename Iter, typename RandomGenerator>
Iter select_randomly(Iter start, Iter end, RandomGenerator &g, bool (*isAvailable)(Card *))
{
    uniform_int_distribution<> dis(0, distance(start, end) - 1);
    advance(start, dis(g));
    //    return isAvailable(*start) ? start : select_randomly(start, end, isAvailable);
    if (isAvailable(*start))
    {
        cout << "[RANDOM] Carte disponible" << endl;
        return start;
    }
    else
    {
        cout << "[RANDOM] Carte non disponible" << endl;
        return select_randomly(start, end, isAvailable);
    }
}

template <typename Iter>
Iter select_randomly(Iter start, Iter end, bool (*isAvailable)(Card *))
{
    static random_device rd;
    static mt19937 gen(rd());
    return select_randomly(start, end, gen, isAvailable);
}

template <class F, class... Args>
void setInterval(atomic_bool &cancelToken, size_t interval, F &&f, Args &&...args)
{
    cancelToken.store(true);
    auto cb = std::bind(forward<F>(f), forward<Args>(args)...);
    async(launch::async, [=, &cancelToken]() mutable
          {
              while (cancelToken.load())
              {
                  cb();
                  this_thread::sleep_for(chrono::milliseconds(interval));
              }
          });
}

bool comparePlayingCards(PLAYING_CARD pc1, PLAYING_CARD pc2)
{
    return (pc1.card->getNbCard() < pc2.card->getNbCard());
}

bool PLAYING_CARD::operator==(const Client &c) const
{
    return client->getId() == c.getId();
}

EndPoint::EndPoint(int connection_port, const int BACKLOG, const int MAXDATASIZE, bool init_winsocks) : connection_port(connection_port), BACKLOG(BACKLOG), init_winsocks(init_winsocks), MAXDATASIZE(MAXDATASIZE), connection_socket(NULL), is_alive(true)
{
}

EndPoint::~EndPoint()
{
    end_thread();
}
int EndPoint::open()
{
    struct sockaddr_in address;
    int yes = 1;

    // Ouverture de la socket de connexion
    if ((connection_socket = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        Output::GetInstance()->print_error("[SERVER] Error while creating connection socket ");
        return false;
    }

    // Configuration de la socket de connexion
    if (setsockopt(connection_socket, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1)
    {
        Output::GetInstance()->print_error("[SERVER] Error while configuring connection socket ");
        return false;
    }

    // Configuration de l'adresse de transport
    address.sin_addr.s_addr = INADDR_ANY;      // adresse, devrait être converti en reseau mais est égal à 0
    address.sin_family = AF_INET;              // type de la socket
    address.sin_port = htons(connection_port); // port, converti en reseau
    bzero(&(address.sin_zero), 8);             // mise a zero du reste de la structure

    // Demarrage du point de connexion : on ajoute l'adresse de transport dans la socket
    if (::bind(connection_socket, (struct sockaddr *)&address, sizeof(struct sockaddr)) == -1)
    {
        Output::GetInstance()->print_error("[SERVER] Error while binding connection socket ");
        return false;
    }

    // Attente sur le point de connexion
    if (listen(connection_socket, BACKLOG) == -1)
    {
        Output::GetInstance()->print_error("[SERVER] Error while listening connection socket ");
        return false;
    }

    return true;
}

int EndPoint::accept_connection()
{
    int client_socket;
    struct sockaddr_in client_address;
    unsigned int sinsize = sizeof(struct sockaddr_in);

    // Acceptation de la connexion
    if ((client_socket = accept(connection_socket, (struct sockaddr *)&client_address, &sinsize)) == -1)
    {
        Output::GetInstance()->print_error("[SERVER] Error while accepting client connection ");
        return NULL;
    }

    if (!is_alive)
        return NULL;

    // Affichage de la connexion
    Output::GetInstance()->print("[SERVER][+] New connection from ", inet_ntoa(client_address.sin_addr), "\n");

    return client_socket;
}

bool EndPoint::close()
{
    if (connection_socket == NULL || !is_alive)
        return true;

    int result;
    Output::GetInstance()->print("[SERVER] Trying to close connection socket...\n");

    result = close();

    if (result == -1)
    {
        Output::GetInstance()->print_error("[SERVER] Error while closing connection socket ");
        Output::GetInstance()->print("\n");
        return false;
    }

    Output::GetInstance()->print("[SERVER] Connection socket closed successfully !\n");

    return true;
}

void EndPoint::execute_thread()
{
    Output::GetInstance()->print("[SERVER] Thread server starts.\n");

    // Ouverture de la socket de connexion
    Output::GetInstance()->print("[SERVER] Trying to open connection socket on the port ", connection_port, "...\n");
    if (!open())
    {
        exit(EXIT_FAILURE);
    }
    Output::GetInstance()->print("[SERVER] Connection socket opened successfully !\n");

    // Création des threads clients call par le serveur

    // Boucle infinie pour le serveur (pour accepter les connexions entrantes)
    int threads_count = 0;
    Client *c;
    while (true)
    {
        if (!is_alive)
            return;

        Output::GetInstance()->print("[SERVER] Waiting for client connection...\n");

        threads_count++;
        int client_socket = accept_connection();
        if (!is_alive)
            return;

        if (client_socket != NULL)
        {
            c = new Client(threads_count, client_socket, MAXDATASIZE, this);
            if (!is_alive)
            {
                c->~Client();
                return;
            }

            c->start_thread();
            clients.push_back(c);
        }
    }
}

void EndPoint::start_thread()
{
    join_thread();
    // Start server thread
    thread = std::thread(&EndPoint::execute_thread, this);
}

void EndPoint::end_thread()
{
    if (!is_alive)
        return;

    Output::GetInstance()->print("[SERVER] Thread server is ending...\n");

    is_alive = false;

    // End all clients
    for (auto it = clients.begin(); it != clients.end(); ++it)
    {
        (*it)->~Client();
    }

    // End thread
    thread.detach();
    thread.~thread();

    // Close end point connection
    close();

    Output::GetInstance()->print("[SERVER] Thread server ends.\n");
}

void EndPoint::join_thread()
{
    if (thread.joinable())
    {
        thread.join();
    }
}

void EndPoint::launchGame()
{
    for (Client *client : EndPoint::incoming_players)
    {
        client->send_message(JSON::serialize("ACK_INIT", "").c_str());
    }
}

void EndPoint::startGame(Client *c)
{
    if (find(this->players.begin(), this->players.end(), c) == this->players.end())
    {
        if (this->incoming_players.size() >= 2 && find(this->incoming_players.begin(), this->incoming_players.end(), c) != this->incoming_players.end())
        {
            this->players.push_back(c);
            if (this->incoming_players.size() == this->players.size() && this->players.size() >= 2)
            {
                if (this->board.empty())
                    this->createBoard();
                for (Client *client : this->players)
                {
                    this->assignPlayerCard(client);
                    sendBoard(client);
                }
            }
        }
        else
        {
            this->incoming_players.at(0)->send_message(JSON::serialize("END", "").c_str());
            this->incoming_players.clear();
            this->players.clear();
            this->startTimer.reset();
        }
    }
}

bool EndPoint::addPlayer(Client *c)
{
    if (this->incoming_players.size() < 10)
    {
        if (this->startTimer)
        {
            if (chrono::system_clock::now() < this->startTimer)
            {
                this->incoming_players.push_back(c);
                return true;
            }
        }
        else
        {
            this->incoming_players.push_back(c);
            return true;
        }
    }
    else
    {
        return false;
    }
}

bool EndPoint::canGameStart()
{
    return this->incoming_players.size() > 1 && this->incoming_players.size() <= 10;
}

bool EndPoint::isPlayer(Client *c)
{
    return find(this->incoming_players.begin(), this->incoming_players.end(), c) != this->incoming_players.end();
}

void EndPoint::launchTimer()
{
    if (!this->startTimer)
    {
        Output::GetInstance()->print("[START TIMER] Initialisation du timer\n");
        atomic_bool cancelToken;
        this->startTimer = chrono::system_clock::now() + chrono::seconds(30);
        setInterval(cancelToken, 1000, [this, &cancelToken, i = 30]() mutable
                    {
                        Output::GetInstance()->print("[START TIMER]", JSON::serialize("START_TIMER", to_string(i).c_str()).c_str(), '\n');
                        for (auto client : this->clients)
                        {
                            stringstream ss;
                            ss << "START_TIMER" << i << endl;
                            client->send_message(JSON::serialize("START_TIMER", to_string(i).c_str()).c_str());
                        }
                        if (i == 0)
                        {
                            cancelToken.store(false);
                            return;
                        }
                        --i;
                    });
    }
    if (chrono::system_clock::now() > this->startTimer)
        this->launchGame();
}

void EndPoint::initCards()
{
    for (int i = 1; i < 105; i++)
    {

        if (i == 55)
        {
            Card *c = new Card(i, 7);
            this->cards.push_back(c);
        }
        else if (i % 10 == 0 && i <= 100)
        {
            Card *c = new Card(i, 3);
            this->cards.push_back(c);
        }
        else if (i == 11 || i == 22 || i == 33 || i == 44 || i == 66 || i == 77 || i == 88 || i == 99)
        {
            Card *c = new Card(i, 5);
            this->cards.push_back(c);
        }
        else
        {
            Card *c = new Card(i, 1);
            this->cards.push_back(c);
        }
    }
}

void EndPoint::assignPlayerCard(Client *c)
{
    while (c->cards.size() < 10)
    {
        Card *card = *select_randomly(this->cards.begin(), this->cards.end(), [](Card *c)
                                      { return !c->getAssigned(); });
        card->setAssigned(true);
        c->cards.push_back(card);
    }
}

string EndPoint::createBoard()
{
    if (this->cards.empty())
        this->initCards();
    while (board.size() < 4)
    {
        Card *card = *select_randomly(this->cards.begin(), this->cards.end(), [](Card *c)
                                      { return !c->getPlayed(); });
        card->setAssigned(true);
        card->setPlayed(true);
        vector<Card *> line;
        line.push_back(card);
        this->board.push_back(line);
    }
    return JSON::serializeBoard(this->board);
}

void EndPoint::playCard(int id, Client *c)
{
    auto it = find(this->playingCards.begin(), this->playingCards.end(), *c);
    if (it == this->playingCards.end())
    {
        for (int i = 0; i < c->cards.size(); ++i)
        {
            if (c->cards.at(i)->getNbCard() == id)
            {
                this->playingCards.push_back({c, c->cards.at(i)});
                c->cards.erase(c->cards.begin() + i);
            }
        }
        if (this->playingCards.size() == this->players.size())
        {
            sort(this->playingCards.begin(), this->playingCards.end(), comparePlayingCards);
            for (auto pc : this->playingCards)
            {
                int diff = 105;
                lineId = 0;
                bool isCardPlayable = false;
                // Choosing a line with the lower difference between client card and last card of each line
                for (int i = 0; i < this->board.size(); ++i)
                {
                    auto line = this->board.at(i);
                    if (line.at(line.size() - 1)->getNbCard() < pc.card->getNbCard() && (pc.card->getNbCard() - line.at(line.size() - 1)->getNbCard()) < diff)
                    {
                        diff = pc.card->getNbCard() - line.at(line.size() - 1)->getNbCard();
                        lineId = i;
                        isCardPlayable = true;
                    }
                }
                // Wait until client choose a line
                if (!isCardPlayable)
                {
                    pc.client->send_message(JSON::serialize("CHOOSE", "").c_str());
                    for (auto player : this->players)
                    {
                        if (player->getId() != pc.client->getId())
                        {
                            player->send_message(JSON::serialize("PATIENT", "").c_str());
                        }
                    }
                    myLock = unique_lock<mutex>(waitClient);
                    playerChoosing.wait(myLock, [this]
                                        { return lkClientChoosing; });
                }
                if (this->board.at(lineId).size() == 5 || !isCardPlayable)
                {
                    pc.client->discarded.insert(pc.client->discarded.end(), this->board.at(lineId).begin(), this->board.at(lineId).end());
                    int value = 0;
                    for (Card *card : pc.client->discarded)
                    {
                        value += card->getHead();
                    }
                    if (value >= 66)
                    {
                        //Player lost
                        pc.client->send_message(JSON::serialize("LOST", "").c_str());
                        for (auto client : this->players)
                        {
                            if (pc.client->getId() != client->getId())
                            {
                                client->send_message(JSON::serialize("WIN", "").c_str());
                            }
                            client->send_message(JSON::serialize("END", "").c_str());
                            this->incoming_players.clear();
                            this->players.clear();
                            this->startTimer.reset();
                        }
                    }
                    this->board.at(lineId).clear();
                }

                this->board.at(lineId).push_back(pc.card);
                pc.card->setPlayed(true);
            }
            for (auto player : this->players)
            {
                sendBoard(player);
            }
            if (this->playingCards.at(0).client->cards.empty())
            {
                for (auto player : this->players)
                {
                    this->assignPlayerCard(player);
                }
            }
            this->playingCards.clear();
        }
    }
}

void EndPoint::chooseLine(int line)
{
    lineId = line;
    lkClientChoosing = true;
    myLock.unlock();
    playerChoosing.notify_one();
}

void EndPoint::sendBoard(Client *c)
{
    if (c == nullptr)
    {
        for (auto *client : this->players)
        {
            client->send_message(JSON::serialize("BOARD", JSON::serializeBoard(this->board).c_str()).c_str());
        }
    }
    else
    {
        c->send_message(JSON::serialize("BOARD", JSON::serializeBoard(this->board).c_str()).c_str());
    }
}

void EndPoint::sendCards(Client *c)
{
    if (c == nullptr)
    {
        for (auto *client : this->players)
        {
            client->send_message(JSON::serialize("CARDS", JSON::serializeCards(client->cards).c_str()).c_str());
        }
    }
    else
    {
        c->send_message(JSON::serialize("CARDS", JSON::serializeCards(c->cards).c_str()).c_str());
    }
}

void EndPoint::sendScore(Client *c)
{
    int value = 0;
    for (Card *card : c->discarded) {
        value += card->getHead();
    }
    c->send_message(JSON::serialize("SCORE", to_string(value).c_str()).c_str());
}

void EndPoint::endPlayer(Client *c)
{
    auto incPlayer = find(this->incoming_players.begin(), this->incoming_players.end(), c);
    auto player = find(this->players.begin(), this->players.end(), c);
    if (incPlayer != this->incoming_players.end())
    {
        this->incoming_players.erase(incPlayer);
    }
    if (player != this->players.end())
    {
        this->players.erase(player);
    }
    if (this->incoming_players.size() == this->players.size() == 1)
    {
        this->incoming_players.at(0)->send_message(JSON::serialize("END", "").c_str());
        this->incoming_players.clear();
        this->players.clear();
        this->startTimer.reset();
    }
}

bool EndPoint::isStartTimerEnded()
{
    return !this->startTimer || std::chrono::system_clock::now() < this->startTimer;
}
