#include <iomanip>
#include <iostream>

#ifndef CARD_H
#define CARD_H

class Card {
private:
    int numberOfCard;
    bool played;
    bool assigned;
	int headOfBeef;

public:
	Card(int, int);
    int getNbCard() {return numberOfCard;}
    int getHead() {return headOfBeef;}
    bool getPlayed() {return played;}
    void setPlayed(bool val) {played = val;}
    bool getAssigned() {return assigned;}
    void setAssigned(bool val) {assigned = val;}

    bool operator==(const Card &rhs) const;

    bool operator!=(const Card &rhs) const;
};

#endif