#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>
#include "../include/rapidjson/document.h"
#include "../include/rapidjson/stringbuffer.h"
#include "../include/rapidjson/prettywriter.h"
#include "Card.h"

#ifndef JSON_H
#define JSON_H

using namespace std;

class JSON
{
private:
  static JSON *singleton_;
  JSON();

public:
  static string serialize(const char *, const char *);
  static string serializeCard(Card);
  static string serializeCards(vector<Card *>);
  static string serializeBoard(vector<vector<Card *>>);
  static JSON *GetInstance();
};

#endif