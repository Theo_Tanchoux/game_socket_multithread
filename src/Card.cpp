#include "Card.h"

Card::Card(int numberOfCard, int headOfBeef) : numberOfCard(numberOfCard), played(false), headOfBeef(headOfBeef)
{
}

bool Card::operator==(const Card &rhs) const
{
    return numberOfCard == rhs.numberOfCard &&
           headOfBeef == rhs.headOfBeef;
}

bool Card::operator!=(const Card &rhs) const
{
    return !(rhs == *this);
}
